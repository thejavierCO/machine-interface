const Serialport = require("serialport");
const { Readline } = Serialport.parsers;
const port = new Serialport("COM5",{baudRate:9600});
const express = require("express");
const morgan = require("morgan");
const socket = require("socket.io");
const app = express();

app.use(morgan("dev"));
app.use(express.json());

app.use("/",express.static(__dirname+"/public"));

let send = socket.listen(app.listen(process.env.PORT||3000,()=>console.log("run")));

send.on("connection",(io)=>{
    console.log(io.id);
})

const parser = port.pipe(new Readline({delimiter:"\r\n"}));
parser.on("open",()=>{console.log("load")});
parser.on("data",(a)=>{
    try{
        send.emit("data",a);
    }catch(err){console.log(err,"err");}
});
port.on("error",(a)=>{
    console.log(a)
})